//
//  HomeViewController.swift
//  NewsApp
//
//  Created by Dibin on 01/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit

var newsArray:NSMutableArray = []

class HomeViewController: UIViewController {
    
    @IBOutlet weak var newsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getLatestNewsFromSever(sourceName: "techcrunch")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func change(_ sender: Any) {
        
//        PopupController.create(self).customize([.animation(.slideUp),.scrollable(false),.backgroundStyle(.blackFilter(alpha: 0.7))]).didShowHandler { popup in
//                print("showed popup!")
//            }
//            .didCloseHandler { _ in
//                print("closed popup!")
//            }
//            .show(categoryViewController.instance())
//        
//        
//        let container = categoryViewController.instance()
//        container.closeHandler = { _ in
//            popup.dismiss()
//        }
        
        
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(true)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = categoryViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        container.delegate = self
        
        popup.show(container)

        }
    func getLatestNewsFromSever(sourceName:String) {
        
        let UrlString:String = "https://newsapi.org/v1/articles?source=\(sourceName)&apiKey=\(constants.APIKEY)"
        
        NewsNetworkEngine.getResponseForUrl(urlString: UrlString) { newsDict in
            
            print(newsDict)
            
            if newsDict.allKeys.count > 0
            {
                
                NewsNetworkEngine.getLiveNewsFromResponse(theNewsDict: newsDict, completion: { allNewsArray in
                    
                    
                    print(allNewsArray)
                    
                    if allNewsArray.count > 0
                    {
                        
                        newsArray = allNewsArray
                        
                        DispatchQueue.main.async {
                            print("main queue")
                            self.newsCollectionView.reloadData()

                            print("colletion view reload")

                            self.newsCollectionView.resetScrollPositionToTop()

                        }
                        
                        
                        
                    }
                })
            }
            
        }
    }
    func pushToDetailedViewController(urlString:String) {
        
        
//        let HomeVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewControllerID") as!
//        HomeViewController
//
//        let navigationVC = UINavigationController(rootViewController: HomeVc)
//        
        
        let detailedVc = self.storyboard?.instantiateViewController(withIdentifier: "DetailedNewsViewControllerID") as!
        DetailedNewsViewController
        
        detailedVc.UrlString = urlString
        
        self.navigationController?.pushViewController(detailedVc, animated: true)
    }
    
    }

extension HomeViewController: categoryDelegate {
    
    func hideCategoryPopup(sourceType: String) {
        
        self.getLatestNewsFromSever(sourceName: sourceType)
    }
}

extension HomeViewController: UICollectionViewDataSource
{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return newsArray.count
        
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let newsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCollectionViewCell
        
        newsCell.updateCell(newsObj: newsArray[indexPath.row] as! LiveNewsModel)
        
        return newsCell
        
        
    }
    
    
}
extension HomeViewController:UICollectionViewDelegateFlowLayout
{
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let selectedObj = newsArray [indexPath.row] as! LiveNewsModel
        
        self.pushToDetailedViewController(urlString:selectedObj.url)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size:CGSize = CGSize(width: self.view.frame.size.width-20, height: 250)
        
        return size
    }

}
extension UIScrollView
{
    func resetScrollPositionToTop() {
        self.contentOffset = CGPoint(x: -contentInset.left, y: -contentInset.top)
    }
}
