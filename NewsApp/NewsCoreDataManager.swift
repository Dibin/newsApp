//
//  NewsCoreDataManager.swift
//  NewsApp
//
//  Created by Dibin on 06/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit
import CoreData
class NewsCoreDataManager: NSObject {
    
 class  func getManagedObjectContext() -> (NSManagedObjectContext) {
        
        let theAppDelegate:AppDelegate = (UIApplication.shared).delegate as! AppDelegate
        
        let context:NSManagedObjectContext = theAppDelegate.persistentContainer.viewContext
        
        return context
        
    }
    
 class func InsertIntoDB(category:newsObject) ->()  {
        
        
        let context:NSManagedObjectContext = self.getManagedObjectContext()
        
        let CategoryDBmodel = NSEntityDescription.insertNewObject(forEntityName: "NewsCategory", into: context) as! NewsCategory
        
        CategoryDBmodel.name = category.mName
        
        CategoryDBmodel.sourceId = category.mId
        
        CategoryDBmodel.imageUrl = category.mImageUrl
        
        
        do
        {
        try context.save()
            
            print("category saved")

        } catch let error as NSError{
            
        print("Error could not save category\(error)")
            
            
            
        }
        
        
    }

    
  class  func getAllCategoriesFromDB(completion:(NSArray)->Void)
    {
        

        var allcategory:[NewsCategory] = []
        
        let context = self.getManagedObjectContext()
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "NewsCategory")
        
        
        do
        {
        
            
            let a = try context.execute(request)
            
        //    completion(a)
            
//           allcategory = try context.execute(request) as! [NewsCategory]
            
            print("results\(allcategory)")
            
            
            
        }catch let error as NSError
        {
        
            print("Error could not fetch\(error)")

        }
        
        
        
    }
    
 class func fetchAllCategories() -> (NSArray) {
    
    var categoriesArray = [NewsCategory]()

        let context = self.getManagedObjectContext()
        
        let request = NSFetchRequest<NewsCategory>(entityName: "NewsCategory")

    
        do
        {
 
            
        categoriesArray = try context.fetch(request)
            
            
        print("results\(categoriesArray)")
            
            
            
        }catch let error as NSError
        {
            
            print("Error could not fetch\(error)")
            
        }

    return categoriesArray as (NSArray)
    }
    
}
