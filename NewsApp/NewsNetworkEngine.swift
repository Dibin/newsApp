//
//  NewsNetworkEngine.swift
//  NewsApp
//
//  Created by Dibin on 02/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit

class NewsNetworkEngine: NSObject {
    
  class func getResponseForUrl(urlString:String,completion:@escaping (NSDictionary) -> ()) {
       
        let theUrl = NSURL(string : urlString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: theUrl as! URL) { data, response, error in

        if (data != nil)
        {
            
            print("got data \(data)")
            
            do {
                let theResultObj:NSDictionary = try JSONSerialization.jsonObject(with: data!) as! [String: AnyObject] as NSDictionary
                
                
                completion(theResultObj)
                
//                let allSourseArray = theResultObj.object(forKey: "sources") as? NSArray
//                
//                print("sourse: \(allSourseArray)")
                
                //  let allSourseArray:NSArray = (theResultObj.objectForKey("nextPageToken") as? NSArray)!
                
                //        let theResultObj = try JSONSerialization.jsonObject(with: data!) as! [NSArray: AnyObject]
                
                print(theResultObj)
                
            } catch {
                print("json error: \(error.localizedDescription)")
            }
            
        }
        
        if (error != nil)
        {
            
            print("no error")
        }
        
        
        print("Task completed")
        // rest of the function...
    }
    //     var theTask = session.dataTask(with: url, completionHandler: (Data?, URLResponse?, Error?) -> Void)
    
    
    task.resume()
        
    
}
    
    
 class func getCategoriesFromResponse(resultObj:NSDictionary,completion:@escaping(NSArray) -> Void) {
        
        
    
         var allcategory:NSMutableArray = []
        
    let allSourseArray = resultObj.object(forKey: "sources") as? NSArray

        print("sourse: \(allSourseArray)")

//        do {
//            let theResultObj:NSDictionary = try JSONSerialization.jsonObject(with: data as Data) as! [String: AnyObject] as NSDictionary
//            
//            
//            
//            //  let allSourseArray:NSArray = (theResultObj.objectForKey("nextPageToken") as? NSArray)!
//            
//            //        let theResultObj = try JSONSerialization.jsonObject(with: data!) as! [NSArray: AnyObject]
//            
//            print(theResultObj)
//            
//        } catch {
//            print("json error: \(error.localizedDescription)")
//        }

        for (_,element) in (allSourseArray?.enumerated())! {
            
            let theNewsObj = newsObject()
            
            let theElement:NSDictionary = (element as? NSDictionary)!
            
            theNewsObj.mCategory = theElement.object(forKey: "category") as! String
            
            print(theNewsObj.mCategory)
            
            theNewsObj.mCountry = theElement.object(forKey: "country") as! String
            
            theNewsObj.mDescription = theElement.object(forKey: "description") as! String
            
            theNewsObj.mId = theElement.object(forKey: "id") as! String
            
            theNewsObj.mLanguage = theElement.object(forKey: "language") as! String
            
            theNewsObj.mName = theElement.object(forKey: "name") as! String
            
            theNewsObj.mUrl = theElement.object(forKey: "url") as! String
            
            theNewsObj.mUrlsToLogos = theElement.object(forKey: "urlsToLogos") as! NSDictionary
            
            
            let logosDict:NSDictionary = theElement.object(forKey: "urlsToLogos") as! NSDictionary

            
            let logoStr:String = (named: logosDict.object(forKey: "medium") as! String)

            theNewsObj.mImageUrl = logoStr
            
            NewsCoreDataManager.InsertIntoDB(category: theNewsObj)
            
          //  allcategory.add(theNewsObj)
            
        }

    
    
 let  arra = NewsCoreDataManager.fetchAllCategories()
    
    completion(arra)
    
    }
    
 class func getLiveNewsFromResponse(theNewsDict:NSDictionary,completion:(NSMutableArray) -> Void ){
        

        let allNewsArray:NSMutableArray = []
        
        let allArticles = theNewsDict.object(forKey: "articles") as! NSArray
        
        for (_,element) in allArticles.enumerated() {
            
          let theLiveNews = LiveNewsModel()
            
            let theElement:NSDictionary = element as! NSDictionary
            
          //  theElement.object(forKey: "author") as! String
            theLiveNews.author = self.checkValueForDictionary(theDict: theElement, Key: "author") ? theElement.object(forKey: "author") as! String : ""
            
            theLiveNews.title = self.checkValueForDictionary(theDict: theElement, Key: "title") ? theElement.object(forKey: "title") as! String : ""
            
            
            theLiveNews.newsDescription = self.checkValueForDictionary(theDict: theElement, Key: "description") ? theElement.object(forKey: "description") as! String : ""
            
            
            theLiveNews.url = self.checkValueForDictionary(theDict: theElement, Key: "url") ? theElement.object(forKey: "url") as! String : ""
                

            theLiveNews.urlToImage = self.checkValueForDictionary(theDict: theElement, Key: "urlToImage") ? theElement.object(forKey: "urlToImage") as! String : ""
                

            theLiveNews.publishedAt =  self.checkValueForDictionary(theDict: theElement, Key: "publishedAt") ? theElement.object(forKey: "publishedAt") as! String : ""
                
            
            allNewsArray.add(theLiveNews)
            
        }
    
completion(allNewsArray)
    
    }
    
 class func checkValueForDictionary(theDict:NSDictionary,Key: String) -> Bool {
        
        
        if theDict[Key] != nil   {
            
            if theDict[Key] is NSNull {
                
                return false

            }
            
            return true
        }
         return false
    }
}
    

