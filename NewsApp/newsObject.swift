//
//  newsObject.swift
//  NewsApp
//
//  Created by Dibin on 12/12/16.
//  Copyright © 2016 lookup. All rights reserved.
//

import UIKit

class newsObject: NSObject {
    
    var mCategory:String = ""
    
    var mCountry:String = ""

    var mDescription:String = ""
    
    var mId:String = ""

    var mLanguage:String = ""

    var mName:String = ""
    
    var mUrl:String = ""
    
    var mUrlsToLogos:NSDictionary = [:]

    var mImageUrl:String = ""


}
