//
//  DetailedNewsViewController.swift
//  NewsApp
//
//  Created by Dibin on 08/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit

class DetailedNewsViewController: UIViewController {

    var UrlString:String? = nil
    
    @IBOutlet weak var detailedWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UrlString != nil) {
            
            detailedWebView.loadRequest(NSURLRequest (url: NSURL (string: UrlString!) as! URL) as URLRequest)

        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        
        detailedWebView.scrollView.contentInset = UIEdgeInsets.zero
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
