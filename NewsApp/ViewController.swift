//
//  ViewController.swift
//  NewsApp
//
//  Created by Dibin on 12/12/16.
//  Copyright © 2016 lookup. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let allNewsArray:NSMutableArray = []
    
    @IBOutlet weak var mCategoryCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

    self.getAllNews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getAllNews()  {
        
        
        let UrlString = "https://newsapi.org/v1/sources"
        
        
        let url = NSURL(string: UrlString)
        
        
        let session = URLSession.shared
        
        
//        var task = session.dataTask(with: url, completionHandler: { data, response, error) -> Void
//            in
//        }
        
        let task = session.dataTask(with: url as! URL) { data, response, error in
            
            
            
            if (data != nil)
            {
            
                print("got data \(data)")
                
     do {
        let theResultObj:NSDictionary = try JSONSerialization.jsonObject(with: data!) as! [String: AnyObject] as NSDictionary
        
        
        let allSourseArray = theResultObj.object(forKey: "sources") as? NSArray
        
        print("sourse: \(allSourseArray)")

      //  let allSourseArray:NSArray = (theResultObj.objectForKey("nextPageToken") as? NSArray)!
        
//        let theResultObj = try JSONSerialization.jsonObject(with: data!) as! [NSArray: AnyObject]

                    print(theResultObj)
        
        self.parseResult(resultArray: allSourseArray!)
                } catch {
                    print("json error: \(error.localizedDescription)")
                }
                
            }
            
            if (error != nil)
            {
            
                print("no error")
            }
            
            
            print("Task completed")
            // rest of the function...
        }
   //     var theTask = session.dataTask(with: url, completionHandler: (Data?, URLResponse?, Error?) -> Void)
        
        
        task.resume()
        
    }
    
    func parseResult(resultArray : NSArray)  {
        
        
        
        for (index,element) in resultArray.enumerated() {
            
            let theNewsObj = newsObject()
            
            let theElement:NSDictionary = (element as? NSDictionary)!
            
            theNewsObj.mCategory = theElement.object(forKey: "category") as! String
            
            print(theNewsObj.mCategory)
            
            theNewsObj.mCountry = theElement.object(forKey: "country") as! String

            theNewsObj.mDescription = theElement.object(forKey: "description") as! String
            
            theNewsObj.mId = theElement.object(forKey: "id") as! String
            
            theNewsObj.mLanguage = theElement.object(forKey: "language") as! String
            
            theNewsObj.mName = theElement.object(forKey: "name") as! String
            
            theNewsObj.mUrl = theElement.object(forKey: "url") as! String
            
            theNewsObj.mUrlsToLogos = theElement.object(forKey: "urlsToLogos") as! NSDictionary
            
            allNewsArray.add(theNewsObj)

        }
        
        mCategoryCollectionView.reloadData()
  
   }
}
extension ViewController: UICollectionViewDataSource
{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
       return allNewsArray.count
    }
 public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
 {
    let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "theCell", for: indexPath) as! CustomCollectionViewCell
    

    let tempNewsObj:newsObject = allNewsArray[indexPath.row] as! newsObject
    
    collectionViewCell.mTitleLabel.text = tempNewsObj.mCategory
    
   
    collectionViewCell.updateCell(newsObj: tempNewsObj)
    
    
//    let imgUrl = NSURL(string: logoStr)
//    
//    let imageData = NSData(contentsOf: imgUrl as! URL)
//    
//    let logoImage = UIImage(data: imageData as! Data)
//    
//    
//    collectionViewCell.mLogoImageView.image = logoImage
    
    
    
    
   // let logoImage:UIImage = UIImage.init(named: logosDict.object(forKey: "medium") as! String)!
    
  //  collectionViewCell.mLogoImageView.image = logoImage
    
    return collectionViewCell

    
    }
}
