//
//  NewsCategory+CoreDataProperties.swift
//  
//
//  Created by Dibin on 06/02/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension NewsCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NewsCategory> {
        return NSFetchRequest<NewsCategory>(entityName: "NewsCategory");
    }

    @NSManaged public var imageUrl: String?
    @NSManaged public var name: String?
    @NSManaged public var sourceId: String?

}
