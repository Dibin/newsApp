//
//  categoryViewController.swift
//  NewsApp
//
//  Created by Dibin on 01/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit
protocol categoryDelegate {
    
    func hideCategoryPopup(sourceType:String)
}
class categoryViewController: UIViewController {

    var closeHandler: (() -> Void)?

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    var allNewsArray:NSArray = []
    var delegate:categoryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.allNewsArray = NewsCoreDataManager.fetchAllCategories()
        
        if allNewsArray.count == 0 {
            
        self.getCategoriesFromServer()
            
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func instance() -> categoryViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        return storyboard .instantiateViewController(withIdentifier:"categoryViewControllerID") as! categoryViewController
    }

    func getCategoriesFromServer() {
        
        let UrlString:String = "https://newsapi.org/v1/sources"
        
        NewsNetworkEngine.getResponseForUrl(urlString: UrlString){ theDict in
            
            print(theDict)
            
            
            NewsNetworkEngine.getCategoriesFromResponse(resultObj: theDict, completion: { categoriesArray in
                
                
                //  print(categoriesArray)
                
                self.allNewsArray = categoriesArray
                
                print("got response")
                
                self.categoryCollectionView .reloadData()
                
            })
            
            
            
            
        }
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension categoryViewController: PopupContentViewController {
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 300,height: 400)
    }
}

extension categoryViewController: UICollectionViewDataSource
{
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
    return allNewsArray.count
        
 //  return 10
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "theCell", for: indexPath) as! CustomCollectionViewCell
        
        
        let tempNewsObj:NewsCategory = allNewsArray[indexPath.row] as! NewsCategory
        
        
        collectionViewCell.updateCell(newsObj: tempNewsObj)
        
        
        //    let imgUrl = NSURL(string: logoStr)
        //
        //    let imageData = NSData(contentsOf: imgUrl as! URL)
        //
        //    let logoImage = UIImage(data: imageData as! Data)
        //
        //
        //    collectionViewCell.mLogoImageView.image = logoImage
        
        
        
        
        // let logoImage:UIImage = UIImage.init(named: logosDict.object(forKey: "medium") as! String)!
        
        //  collectionViewCell.mLogoImageView.image = logoImage
        
        return collectionViewCell
        
        
    }
}
extension categoryViewController:UICollectionViewDelegate
{

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
    
        let selectedSource = self.allNewsArray [indexPath.row] as! NewsCategory
        
        print("source\(selectedSource.sourceId)")
        
        
        closeHandler?()


        if let delegate = self.delegate
        {
        
                delegate.hideCategoryPopup(sourceType: selectedSource.sourceId!)



        
        }
    }
}
