//
//  CustomCollectionViewCell.swift
//  NewsApp
//
//  Created by Dibin on 13/12/16.
//  Copyright © 2016 lookup. All rights reserved.
//

import UIKit
import SDWebImage
class CustomCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var mLogoImageView: UIImageView!
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5.0
        
        self.layer.borderWidth = 1.0
        
        self.layer.borderColor = UIColor.gray.cgColor
    }
    
    func updateCell(newsObj: NewsCategory)  {
        
//    
//        let logosDict:NSDictionary = newsObj.mUrlsToLogos as NSDictionary
//        
//        let logoStr:String = (named: logosDict.object(forKey: "medium") as! String)
        
        let imageUrl = URL(string: newsObj.imageUrl!)
        
     
        /*let data = try? Data(contentsOf: imageUrl!)
        

        
        let theImage = UIImage(data: data!, scale: 1.0)
        
        mLogoImageView.image = theImage*/
        
    /*    mLogoImageView .sd_setImage(with: imageUrl as URL!, placeholderImage: UIImage(named: "house.png"))
        
        
        let block: SDWebImageCompletionBlock = {(image, error, cacheType, imageURL) -> Void in
           // activityIndicator.stopAnimating()
        }
        
        mLogoImageView.sd_setImage(with: imageUrl as URL!, placeholderImage: placeholderImage:UIImage(named: "house.png"), completed:block
        )*/
    
     /*   let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
        }
       
        mLogoImageView.sd_setImage(with: imageUrl as URL!, placeholderImage: UIImage(named: "house.png"), completed:{ (image, error, cacheType, imageURL) in
            // Perform operation.
        })
        
        mLogoImageView.sd_setImage(with: imageUrl as URL!, placeholderImage: UIImage(named: "house.png"), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
        })*/
        
       
     /*   mLogoImageView.sd_setImage(with: imageUrl as URL!) { (image, error, imageCacheType, imageUrl) in
            if image != nil {
                print("image found")
            }else
            {
                print("image not found")
            }
        }*/
        
        
        print("imageUrl:\(imageUrl)")
        
    /*    let block: SDWebImageCompletionBlock = {(image, error, cacheType, imageUrl) -> Void in
            
            if (imageUrl != nil) {
                
                if (image != nil) {
                    
                    
                }
            }

        }*/
      /*  mLogoImageView.sd_setImage(with: imageUrl as URL!, completed:{(image, error, cacheType, imageUrl) -> Void in
            
            if (imageUrl != nil) {
                
                if (image != nil) {
                    
                    
                }
            }
 
        })*/
        
         /*      mLogoImageView?.sd_setImage(with: imageUrl as URL!, placeholderImage: UIImage(named:"house.png"),
                         options: [.continueInBackground, .lowPriority]) { (image, error, cacheType, url) in
                
                            
                            if (error != nil)
                            {
                            
                            }
                            if (imageUrl != nil) {
                                
                                if (image != nil) {
                                    
                                    
                                }
                            }
                            
                            

        }*/
        
        mLogoImageView.sd_setImage(with: imageUrl as URL!, placeholderImage: UIImage(named:"house.png"), options: SDWebImageOptions(), completed: { (image, error, cacheType, url) in
            
            
            if (error != nil)
            {
                
                print("Error:\(error)")
                
            }
            if (imageUrl != nil) {
                
                if (image != nil) {
                    
                    
                }
            }
            
            
            
        })

     //   mLogoImageView.sd_setImage(with: imageUrl as URL!, placeholderImage:  UIImage(named: "house.png"))
    }
}
