//
//  NewsCollectionViewCell.swift
//  NewsApp
//
//  Created by Dibin on 07/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellContainerView: UIView!
    
    @IBOutlet weak var cellBackgroungImageView: UIImageView!
    
    @IBOutlet weak var titleLabel_Height_Constraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        
        
        cellContainerView.layer.cornerRadius = 5.0
        
//        cellBackgroungImageView.layer.cornerRadius = 10.0
//        
//        cellBackgroungImageView.layer.masksToBounds = true
    }
    
    func updateCell(newsObj:LiveNewsModel)  {
        
        
       cellBackgroungImageView.sd_setImage(with: NSURL(string: newsObj.urlToImage) as URL!, placeholderImage: nil)
        
        self.heightForLAbel(text: newsObj.title)
        
      //  titleLabel.text = newsObj.title
        
        descriptionLabel.text = newsObj.newsDescription
    }
    
    func heightForLAbel(text:String) {
        
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        titleLabel.sizeToFit()
        
        titleLabel.text = text
        
        let h = titleLabel.frame.height
        
        titleLabel_Height_Constraint.constant = h
        
        
    }

}
