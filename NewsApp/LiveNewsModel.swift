//
//  LiveNewsModel.swift
//  NewsApp
//
//  Created by Dibin on 07/02/17.
//  Copyright © 2017 lookup. All rights reserved.
//

import UIKit

class LiveNewsModel: NSObject {
    
    
    var author:String = ""
    
    var title:String = ""
    
    var newsDescription:String = ""
    
    var url:String = ""
    
    var urlToImage:String = ""
    
    var publishedAt:String = ""
    
    
}
